const express = require('express');
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
// var DeleteAllData = require('./function/deleteAll');
// var GetandUploadAllData = require('./function/GetandUploadData');

// bind routers-----
const company_code_router = require('./routes/company_code');
const route_router = require('./routes/route');
const rstop_router = require('./routes/rstop');
const stop_router = require('./routes/stop');
const speedmap_router = require('./routes/speedmap');
const sectionFare_router = require('./routes/sectionFare');

// routers----
app.use('/company_code', company_code_router);
app.use('/route', route_router);
app.use('/rstop', rstop_router);
app.use('/stop', stop_router);
app.use('/speedmap', speedmap_router);
app.use('/sectionFare', sectionFare_router);

// function Run(){

//     var check = 0;

//     if(check = 0){

//         var deleteAlldata = function() {
//             return DeleteAllData;
//         }

//         module.exports = {
//             deleteAlldata: deleteAlldata
//         }
//     }

//     var Upload = function() {
//         return GetandUploadAllData;
//     }

//     module.exports = {
//         Upload: Upload
//     }

// }

// Run();


// swagger router
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(3000, () => console.log('App listening on port 3000!'));

module.exports = app;
