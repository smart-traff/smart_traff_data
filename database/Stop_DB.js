var Connection = require("./Connection");

class Stop_DB extends Connection {
    constructor() {
        super("smart-traffic-testdb", "BusStoplocation");
    }

    async getByID(id) {
        return await this.collection.findOne({"STOP_ID": parseInt(id)});
    }
}

module.exports = Stop_DB;
