var Connection = require("./Connection");

class Rstop_DB extends Connection {
    constructor() {
        super("smart-traffic-testdb", "rstop");
    }

    async getByRouteID(routeID) {
        return await this.collection.find({"ROUTE_ID": parseInt(routeID)}).toArray();
    }

    async getByStopID(stopID) {
        return await this.collection.find({"STOP_ID": parseInt(stopID)}).toArray();
    }    

    async getByRouteID_RouteSeq(routeID, route_seq) {
        return await this.collection.find({"ROUTE_ID": parseInt(routeID), "ROUTE_SEQ": parseInt(route_seq)}).sort({ ROUTE_SEQ: 1, STOP_SEQ: 1 }).toArray();
    }

    async getByRouteID_StopSeq(routeID, seq) {
        return await this.collection.findOne({"ROUTE_ID": parseInt(routeID), "STOP_SEQ": parseInt(seq)});
    }
}

module.exports = Rstop_DB;
