class Connection {
    collection;

    constructor(dbName, table) {
        this.dbName = dbName;
        this.table = table;
    }

    connect(client) {
        this.collection = client.db(this.dbName).collection(this.table);
    }

    async getAll() {        
        return await this.collection.find().toArray();
    }
}

module.exports = Connection;
