const { MongoClient } = require("mongodb");

class Client {
    #client;
    
    async connect() {
        this.#client = new MongoClient("mongodb+srv://ccfchan:0gkkvm123@cluster0.irxvt.mongodb.net/Test1?retryWrites=true&w=majority", { useUnifiedTopology: true });
        await this.#client.connect();
    }

    get client() {
        return this.#client;
    }

    get db() {
        return this.#client.db();
    }

    db(name) {
        return this.#client.db(name)
    }
}

module.exports = Client;