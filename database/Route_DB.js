var Connection = require("./Connection");

class Route_DB extends Connection {
    constructor() {
        super("smart-traffic-testdb", "route");
    }

    async getByID(id) {
        return await this.collection.findOne({"ROUTE_ID": parseInt(id)});
    }

    async getByCompanyCode(code) {
        return await this.collection.find({"COMPANY_CODE": code}).toArray();
    }

    async getByCompanyCode_Name(code, name) {        
        return await this.collection.findOne({"COMPANY_CODE": code, "ROUTE_NAMEC": name});
    }

    async getByName(name) {
        return await this.collection.find({"ROUTE_NAMEC": name}).toArray();
    }
}

module.exports = Route_DB;
