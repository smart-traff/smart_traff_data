var Connection = require("./Connection");

class SectionFare_DB extends Connection {
    constructor() {
        super("smart-traffic-testdb", "section_fare");
    }

    async getByOnOffSeq(id, seq, on, off) {
        return await this.collection.findOne({"ROUTE_ID": parseInt(id), "ROUTE_SEQ": parseInt(seq), "ON_SEQ": parseInt(on), "OFF_SEQ": parseInt(off)});
    }
}

module.exports = SectionFare_DB;
