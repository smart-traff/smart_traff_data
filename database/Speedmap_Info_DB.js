var Connection = require("./Connection");

class Speedmap_Info_DB extends Connection {
    constructor() {
        super("smart-traffic-testdb", "speedmap_info");
    }

    async getByLinkID(linkID) {
        return await this.collection.findOne({"Link ID": linkID});
    }
}

module.exports = Speedmap_Info_DB;
