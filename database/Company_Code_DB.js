var Connection = require("./Connection");

class Company_Code_DB extends Connection {
    constructor() {
        super("smart-traffic-testdb", "company_code");
    }

    async getByCompanyCode(code) {
        return await this.collection.findOne({"COMPANY_CODE": code});
    }
}

module.exports = Company_Code_DB;
