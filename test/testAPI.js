const supertest = require("supertest");
const chai = require("chai");
const should = require("should");
const app = require("../index");

chai.should();
chai.use(require('chai-things'));

const request = supertest(app);

describe("API", async () => {

    before("waiting server", async () => {
        await new Promise(resolve => setTimeout(resolve, 5000));
    });

    describe("/company_code", async () => {
        it("/               | should respond all company record", async () => {
            const data = (await request.get("/company_code")).body;
            data.length.should.above(0);
        });
        
        it("/{COMPANY_CODE} | should respond with matching records when using COMPANY_CODE", async () => {
            const expected_company_code = "CTB";
            const data = (await request.get(`/company_code/${expected_company_code}`)).body;
            data["COMPANY_CODE"].should.equal(expected_company_code);
        });
    });

    describe("/route", async() => {
        it("/                               | should respond all route record", async () => {
            const data = (await request.get("/route")).body;
            data.length.should.above(0);
        });
        
        it("/{ROUTE_ID}                     | should respond with matching records when using ROUTE_ID", async () => {
            const expected_route_id = 1038;
            const data = (await request.get(`/route/${expected_route_id}`)).body;
            data["ROUTE_ID"].should.equal(expected_route_id);
        });

        it("/company_code/{COMPANY_CODE}    | should respond with matching records when using COMPANY_CODE", async () => {
            const expected_company_code = "CTB";
            const data = (await request.get(`/route/company_code/${expected_company_code}`)).body;
            data.should.all.have.property("COMPANY_CODE", expected_company_code);
        });

        it("/name/{ROUTE_NAME}              | should respond with matching records when using ROUTE_NAME", async () => {
            const expected_route_name = "14";
            const data = (await request.get(`/route/name/${expected_route_name}`)).body;
            data.should.all.have.property("ROUTE_NAMEC", expected_route_name);
        });
    });

    describe("/rstop", async() => {
        it("/{ROUTE_ID}                        | should respond with matching records when using ROUTE_ID", async () => {
            const expected_route_id = 1038;
            const data = (await request.get(`/rstop/${expected_route_id}`)).body;
            data.should.all.have.property("ROUTE_ID", expected_route_id);
        });
        
        it("/stopID/{STOP_ID}                  | should respond with matching records when using STOP_ID", async () => {
            const expected_stop_id = 12368;
            const data = (await request.get(`/rstop/stopID/${expected_stop_id}`)).body;
            data.should.all.have.property("STOP_ID", expected_stop_id);
        });
        
        it("/{ROUTE_ID}/{ROUTE_SEQ}/{STOP_SEQ} | should respond empty from kmb api with invalid ROUTE_ID", async () => {
            const kmb_route_id = -1;
            const route_seq = 1;
            const stop_seq = 1;
            const data = await request.get(`/rstop/${kmb_route_id}/${route_seq}/${stop_seq}`);
            should.exist(data);
        });

        it("/{ROUTE_ID}/{ROUTE_SEQ}/{STOP_SEQ} | should respond empty from ctb/nwfb api with invalid ROUTE_ID", async () => {
            const ctb_route_id = -1;
            const route_seq = 2;
            const stop_seq = 1;
            const data = await request.get(`/rstop/${ctb_route_id}/${route_seq}/${stop_seq}`);
            should.exist(data);
        });

        it("/{ROUTE_ID}/{ROUTE_SEQ}/{STOP_SEQ} | should respond empty from api with invalid ROUTE_SEQ", async () => {
            const kmb_route_id = 1038;
            const route_seq = -1;
            const stop_seq = 1;
            const data = await request.get(`/rstop/${kmb_route_id}/${route_seq}/${stop_seq}`);
            should.exist(data);
        });

        it("/{ROUTE_ID}/{ROUTE_SEQ}/{STOP_SEQ} | should have respond from kmb api", async () => {
            const kmb_route_id = 1038;
            const route_seq = 1;
            const stop_seq = 1;
            const data = await request.get(`/rstop/${kmb_route_id}/${route_seq}/${stop_seq}`);
            should.exist(data);
        });

        it("/{ROUTE_ID}/{ROUTE_SEQ}/{STOP_SEQ} | should have respond from ctb api", async () => {
            const ctb_route_id = 1495;
            const route_seq = 2;
            const stop_seq = 1;
            const data = await request.get(`/rstop/${ctb_route_id}/${route_seq}/${stop_seq}`);
            should.exist(data);
        });

        it("/{ROUTE_ID}/{ROUTE_SEQ}/{STOP_SEQ} | should have respond from nwfb api", async () => {
            const ctb_route_id = 1509;
            const route_seq = 1;
            const stop_seq = 1;
            const data = await request.get(`/rstop/${ctb_route_id}/${route_seq}/${stop_seq}`);
            should.exist(data);
        });
    });

    describe("/sectionFare", async() => {
        it("/{ROUTE_ID}/{ROUTE_SEQ}/{ON_SEQ}/{OFF_SEQ}  | should respond with matching records when using ROUTE_ID, ROUTE_SEQ, ON_SEQ & OFF_SEQ", async () => {
            const expected_route_id = 1038;
            const expected_route_seq = 2;
            const expected_on_seq = 4;
            const expected_off_seq = 5;
            const expected_price = 6.8;
            const data = (await request.get(`/sectionFare/${expected_route_id}/${expected_route_seq}/${expected_on_seq}/${expected_off_seq}`)).body;
            data["ROUTE_ID"].should.equal(expected_route_id);
            data["ROUTE_SEQ"].should.equal(expected_route_seq);
            data["ON_SEQ"].should.equal(expected_on_seq);
            data["OFF_SEQ"].should.equal(expected_off_seq);
            data["PRICE"].should.equal(expected_price);
        });
    });

    describe("/stop", async() => {
        it("/                        | should respond with matching records when using ROUTE_ID", async () => {
            const data = (await request.get(`/stop`)).body;
            data.length.should.above(0);
        });

        it("/{STOP_ID}                        | should respond with matching records when using ROUTE_ID", async () => {
            const expected_stop_id = 12368;
            const data = (await request.get(`/stop/${expected_stop_id}`)).body;
            data["STOP_ID"].should.equal(expected_stop_id);
            data.should.have.property("E");
            data.should.have.property("N");
        });

        it("/{COMPANY_CODE}/{ROUTE_NAME}                | should respond the start stop and end stop when using COMPANY_CODE & ROUTE_NAME", async () => {
            const expected_company_code = "KMB";
            const expected_route_name = "14";
            const data = (await request.get(`/stop/${expected_company_code}/${expected_route_name}`)).body;
            data.should.have.property(expected_company_code);
            data[expected_company_code].should.all.have.property("Name", expected_route_name);
            data[expected_company_code].should.all.have.property("N");
            data[expected_company_code].should.all.have.property("E");
        });

        it("/{COMPANY_CODE}/{ROUTE_NAME}                | should respond empty when using invalid data", async () => {
            const expected_company_code = "test";
            const expected_route_name = "14";
            const data = (await request.get(`/stop/${expected_company_code}/${expected_route_name}`)).body;
            should.exist(data);
            should.not.exist(data[expected_company_code]);
        });

        it("/{COMPANY_CODE}/{ROUTE_NAME}/{ROUTE_SEQ}    | should respond with matching stop location when using COMPANY_CODE, ROUTE_NAME & ROUTE_SEQ", async () => {
            const expected_company_code = "KMB";
            const expected_route_name = "14";
            const expected_route_seq = 1;
            const data = (await request.get(`/stop/${expected_company_code}/${expected_route_name}/${expected_route_seq}`)).body;
            data.should.have.property(expected_company_code);
            data[expected_company_code].should.all.have.property("Name", expected_route_name);
            data[expected_company_code].should.all.have.property("N");
            data[expected_company_code].should.all.have.property("E");
        });

        it("/{COMPANY_CODE}/{ROUTE_NAME}/{ROUTE_SEQ}    | should respond empty with invalid COMPANY_CODE", async () => {
            const expected_company_code = "test";
            const expected_route_name = "14";
            const expected_route_seq = 1;
            const data = (await request.get(`/stop/${expected_company_code}/${expected_route_name}/${expected_route_seq}`)).body;
            should.exist(data);
            should.not.exist(data[expected_company_code]);
        });

        it("/{COMPANY_CODE}/{ROUTE_NAME}/{ROUTE_SEQ}    | should respond empty with invalid ROUTE_NAME", async () => {
            const expected_company_code = "KMB";
            const expected_route_name = "-1";
            const expected_route_seq = 1;
            const data = (await request.get(`/stop/${expected_company_code}/${expected_route_name}/${expected_route_seq}`)).body;
            should.exist(data);
            should.not.exist(data[expected_company_code]);
        });
    });
    
    describe("/speedmap", async() => {
        it("/           | should respond all traffic status", async () => {
            const data = (await request.get("/speedmap")).body;
            data.length.should.equal(608);
        });

        it("/{LINK_ID}  | should respond with matching records when using LINK_ID", async () => {
            const expected_link_id = "793-877";
            const data = (await request.get(`/speedmap/${expected_link_id}`)).body;
            data["Link ID"].should.equal(expected_link_id);
            data["Region"].should.equal("HK");
            data["Road Type"].should.equal("MAJOR ROUTE");
        });
    });
});
