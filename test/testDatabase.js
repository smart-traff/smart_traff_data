const { MongoClient } = require("mongodb");
const should = require("should");
const Client = require("../database/Client");
const Company_Code_DB = require("../database/Company_Code_DB");
const Route_DB = require("../database/Route_DB");
const Rstop_DB  = require("../database/Rstop_DB");
const SectionFare_DB  = require("../database/SectionFare_DB");
const Speedmap_Info_DB  = require("../database/Speedmap_Info_DB");
const Stop_DB  = require("../database/Stop_DB");

describe("Database", async () => {
    const client = new Client();
    
    before(async () => {            
        await client.connect();
    });

    it("should connect to cloud" , function() {
        should.exist(client.db);
    });

    describe("Company Code", async () => {
        const company_code_db = new Company_Code_DB();

        before(function() {
            company_code_db.connect(client);
        });

        it("should respond all record", async () => {
            const company_code_data = await company_code_db.getAll();
            const expected_count = await client.db("smart-traffic-testdb").collection("company_code").countDocuments();
            company_code_data.length.should.equal(expected_count);
        });

        it("should respond with matching records when using COMAPNY_CODE", async () => {
            const expected_company_code = "KMB";
            const kmb_data = await company_code_db.getByCompanyCode(expected_company_code);
            kmb_data["COMPANY_CODE"].should.equal(expected_company_code);
        });

        it("should return null if no record found", async () => {
            const null_data_getByCompanyCode = await company_code_db.getByCompanyCode("null");
            should.not.exist(null_data_getByCompanyCode);
        });
    });

    describe("Route", async () => {
        const route_db = new Route_DB();
        let cloudData;

        before(function() {
            route_db.connect(client);
            cloudData = client.db("smart-traffic-testdb").collection("route");
        });

        it("should respond all record", async () => {
            const route_data = await route_db.getAll();
            const expected_count = await cloudData.countDocuments();
            route_data.length.should.equal(expected_count);
        });

        it("should respond with matching records when using ROUTE_ID", async () => {
            const expected_route_id = 1038;
            const route_data = await route_db.getByID(expected_route_id);
            route_data["ROUTE_ID"].should.equal(expected_route_id);
        });

        it("should respond with matching records when using COMPANY_CODE", async () => {
            const expected_company_code = "KMB"
            const route_data = await route_db.getByCompanyCode(expected_company_code);
            const expected_count = await cloudData.countDocuments({COMPANY_CODE: expected_company_code});
            route_data.should.all.have.property("COMPANY_CODE", expected_company_code);
            route_data.length.should.equal(expected_count);
        });

        it("should respond with matching records when using ROUTE_NAME", async () => {
            const expected_route_name = "14";
            const route_data = await route_db.getByName(expected_route_name);
            const expected_count = await cloudData.countDocuments({ROUTE_NAMEC: expected_route_name});
            route_data.should.all.have.property("ROUTE_NAMEC", expected_route_name);
            route_data.length.should.equal(expected_count);
        });

        it("should return null if no record found", async () => {
            const null_data_getByID = await route_db.getByID(-1);
            const null_data_getByCompanyCode = await route_db.getByCompanyCode("null");
            const null_data_getByName = await route_db.getByName("null");
            should.not.exist(null_data_getByID);
            null_data_getByCompanyCode.length.should.equal(0);
            null_data_getByName.length.should.equal(0);
        });
    });

    describe("Rstop", async () => {
        const rstop_db = new Rstop_DB();
        let cloudData;

        before(function() {
            rstop_db.connect(client);
            cloudData = client.db("smart-traffic-testdb").collection("rstop");
        });

        it("should respond with matching records when using ROUTE_ID", async () => {
            const expected_route_id = 1038;
            const rstop_data = await rstop_db.getByRouteID(expected_route_id);
            const expected_count = await cloudData.countDocuments({ROUTE_ID: expected_route_id});
            rstop_data.should.all.have.property("ROUTE_ID", expected_route_id);
            rstop_data.length.should.equal(expected_count);
        });

        it("should respond with matching records when using STOP_ID", async () => {
            const expected_stop_id = 12368;
            const rstop_data = await rstop_db.getByStopID(expected_stop_id);
            const expected_count = await cloudData.countDocuments({STOP_ID: expected_stop_id});
            rstop_data.should.all.have.property("STOP_ID", expected_stop_id);
            rstop_data.length.should.equal(expected_count);

        });

        it("should respond with matching records when using ROUTE_ID & ROUTE_SEQ", async () => {
            const expected_route_id = 1038;
            const expected_route_seq = 1;
            const rstop_data = await rstop_db.getByRouteID_RouteSeq(expected_route_id, expected_route_seq);
            const expected_count = await cloudData.countDocuments({ROUTE_ID: expected_route_id, ROUTE_SEQ: expected_route_seq});
            rstop_data.should.all.have.property("ROUTE_ID", expected_route_id);
            rstop_data.should.all.have.property("ROUTE_SEQ", expected_route_seq);
            rstop_data.length.should.equal(expected_count);
        });

        it("should return null if no record found", async () => {
            const null_data_getByRouteID = await rstop_db.getByRouteID(-1);
            const null_data_getByStopID = await rstop_db.getByStopID(-1);
            const null_data_getByRouteID_StopSeq = await rstop_db.getByRouteID_StopSeq(-1);
            null_data_getByRouteID.length.should.equal(0);
            null_data_getByStopID.length.should.equal(0);
            should.not.exist(null_data_getByRouteID_StopSeq);
        });
    });

    describe("SectionFare", async () => {
        const sectionFare_db = new SectionFare_DB();

        before(function() {
            sectionFare_db.connect(client);
        });

        it("should respond with matching records when using ROUTE_ID, ROUTE_SEQ, ON_SEQ & OFF_SEQ", async () => {
            const expected_route_id = 1038;
            const expected_route_seq = 2;
            const expected_on_seq = 1;
            const expected_off_seq = 4;
            const section_fare_data = await sectionFare_db.getByOnOffSeq(
                expected_route_id,
                expected_route_seq,
                expected_on_seq,
                expected_off_seq
            );
            section_fare_data["ROUTE_ID"].should.equal(expected_route_id);
            section_fare_data["ROUTE_SEQ"].should.equal(expected_route_seq);
            section_fare_data["ON_SEQ"].should.equal(expected_on_seq);
            section_fare_data["OFF_SEQ"].should.equal(expected_off_seq);
        });

        it("should return null if no record found", async () => {
            const expected_route_id = -1;
            const expected_route_seq = 2;
            const expected_on_seq = 1;
            const expected_off_seq = 4;
            const section_fare_data = await sectionFare_db.getByOnOffSeq(
                expected_route_id,
                expected_route_seq,
                expected_on_seq,
                expected_off_seq
            );
            should.not.exist(section_fare_data);
        });
    });

    describe("Stop", async () => {
        const stop_db = new Stop_DB();
        let cloudData;

        before(function() {
            stop_db.connect(client);
            cloudData = client.db("smart-traffic-testdb").collection("BusStoplocation");
        });

        it("should respond all records", async () => {
            const stop_data = await stop_db.getAll();
            const expected_count = await cloudData.countDocuments();
            stop_data.length.should.equal(expected_count);
        });

        it("should respond with matching records when using STOP_ID", async () => {
            const expected_stop_id = 12368;
            const stop_data = await stop_db.getByID(expected_stop_id);
            stop_data["STOP_ID"].should.equal(expected_stop_id);
        });

        it("should return null if no record found", async () => {
            const expected_stop_id = -1;
            const null_data = await stop_db.getByID(expected_stop_id);
            should.not.exist(null_data);
        });

    });

    describe("Speedmap", async () => {
        const speedmap_info_db = new Speedmap_Info_DB();
        let cloudData;

        before(function() {
            speedmap_info_db.connect(client);
            cloudData = client.db("smart-traffic-testdb").collection("speedmap_info");
        });

        it("should respond all records", async () => {
            const data = await speedmap_info_db.getAll();
            const expected_count = await cloudData.countDocuments();
            data.length.should.equal(expected_count);
        });

        it("should respond with matching records when using LINK_ID", async () => {
            const expected_link_id = "756-752";
            const speedmap_info_data = await speedmap_info_db.getByLinkID(expected_link_id);
            speedmap_info_data["Link ID"].should.equal(expected_link_id);
        });
    });
});
