# Smart_Traff_Data

# Installation
npm install express nodemon
npm install axios
npm install mongodb
npm install swagger-jsdoc
npm install swagger-ui-express
npm install xml2js

# MongoDB(Database)
MongoDB is a cross-platform document-oriented database program

# Swagger(API-Documents)
Swagger is an Interface Description Language for describing RESTful APIs expressed using JSON

# Attribute of database description

Company_Code -> (Eng)公司名稱("CTB")

COMPANY_NAMEC -> (Chi)公司名稱("城巴")

ROUTE_NAMEC -> 巴士名稱

LOC_START_NAMEC ->  巴士起始站名(chi)

LOC_END_NAMEC -> 巴士終點站名(chi)

STOP_SEQ -> 相應總線的此停止順序 (14B 第一個站係呢個站, 216M 第四個站先係呢個站)

"STOP_NAMEC" -> 嗰個站慨站名 (擺花街)

STOP_IP -> 站的ID

LINK_ID -> 點對點路號

REGION -> 這條路的區域

ROAD_SATURATION_LEVEL -> 該區域的行車速度水平

TRAFFIC_SPEED -> 行駛速度（公里/小時）

eta_seq -> 即將到來的巴士的順序

eta -> 巴士到達的時間

Rstop -> 巴士站信息

Stop ->  站的位置信息

Route -> 巴士路線

Speedmap -> 主要道路的平均行車速度

# Different path dfinitions ( Traditional Chinese Version)

"/company_code" -> 所有公司名稱

"/company_code/{COMPANY_CODE}" -> 單獨一間公司的資訊

"/route" -> 所有巴士路線 

"/route/{ROUTE_ID}" -> 單獨巴士路線

"/route/name/{ROUTE_NAMEC}" -> 按巴士名稱獲取路線清單

"/route/company_code/{COMPANY_CODE}" -> 按公司獲取路線清單

"/rstop" -> 獲取所有rstop信息

"/rstop/{ROUTE_ID}" -> 通過ROUTE_ID獲取rstop列表

"/rstop/stopID/{STOP_ID}" -> 通過STOP_ID獲得單獨站資訊

"/rstop/{ROUTE_ID}/{STOP_SEQ}" -> 該API使用ROUTE_ID和STOP_SEQ將顯示公交車站在下3個點到達時間

"/stop/" -> 所有站資訊 

"/stop/{STOP_ID}" -> 單獨站位置

"/coordinate/{X}/{Y}" -> 使用X和Y坐標獲取站點位置

"/speedmap" -> 獲取主要道路的所有交通速度

"/speedmap/{LINK_ID}" -> 獲得單獨道路的交通速度


# Feature (kmb-api)
npm intsall js-kmb-api
npm install aes-js
npm install array-flat-polyfill
npm install axios(replaced)
npm install node-stroage-shim
npm install ssl-root-cas
npm install hkscs_converter

Link: https://www.npmjs.com/package/js-kmb-api













