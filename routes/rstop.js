var express = require("express");
var router = express.Router();
const axios = require("axios");
const Kmb = require('js-kmb-api').default;
const Rstop_DB = require("../database/Rstop_DB");
const Route_DB = require("../database/Route_DB");
const SectionFare_DB = require("../database/SectionFare_DB");
const Client = require("../database/Client");
const kmb = new Kmb;

const client = new Client();
const rstop_db = new Rstop_DB();
const route_db = new Route_DB();
const sectionFare_db = new SectionFare_DB();

(async () => {
    await client.connect();
    rstop_db.connect(client);
    route_db.connect(client);
    sectionFare_db.connect(client);
})();

router.get("/:ROUTE_ID", async (req, res) => {
    const stop_data = await rstop_db.getByRouteID(req.params.ROUTE_ID);

    stop_data.filter(function(item, index) {
        if (item["STOP_NAMEC"].includes("/<br/>")) {
            const stop_namec = item["STOP_NAMEC"].split("/<br/>")[1];
            stop_data[index]["STOP_NAMEC"] = stop_namec;
        }
    });
    res.send(stop_data);
});

router.get("/stopID/:STOP_ID", async (req, res) => {
    res.send(await rstop_db.getByStopID(req.params.STOP_ID));
});

router.get("/:ROUTE_ID/:ROUTE_SEQ/:STOP_SEQ", async (req, res) => {
    const routeID = req.params.ROUTE_ID;
    const route_seq = parseInt(req.params.ROUTE_SEQ);
    const stop_seq = parseInt(req.params.STOP_SEQ)
    const route_data = await route_db.getByID(routeID);

    if (route_data === null) {
        res.send("[]");
    } else if (route_seq != 1 && route_seq != 2) {
        res.send("[]");
    } else if (route_data["COMPANY_CODE"] == "CTB" || route_data["COMPANY_CODE"] == "NWFB") {
        const company_code = route_data["COMPANY_CODE"];
        const route_name = route_data["ROUTE_NAMEC"];

        const api_route = route_seq === 1 ?
            await axios.get("https://rt.data.gov.hk//v1/transport/citybus-nwfb/route-stop/" + company_code + "/" + route_name + "/inbound") : 
            await axios.get("https://rt.data.gov.hk//v1/transport/citybus-nwfb/route-stop/" + company_code + "/" + route_name + "/outbound");

        const rstop_data = api_route["data"]["data"];
        
        if (rstop_data.length === 0) {
            res.send("[]")
        } else {
            const rstop = rstop_data.filter(function(item) {
                return item["seq"] == stop_seq;
            });

            if (rstop.length === 0) {
                res.send("[]");
            } else {
                let fare_data = stop_seq == rstop_data.length ?
                    await sectionFare_db.getByOnOffSeq(routeID, oppositeSEQ(route_seq), 1, 2) :
                    await sectionFare_db.getByOnOffSeq(routeID, route_seq, stop_seq, stop_seq + 1);

                if (fare_data == null) {
                    fare_data = [];
                    fare_data["PRICE"] = -1;
                }
                
                const stop_id = rstop[0]["stop"];
                const api_eta = await axios.get("https://rt.data.gov.hk/v1/transport/citybus-nwfb/eta/" + company_code + "/" + stop_id + "/" + route_name);
                api_eta["data"]["data"].forEach(function(element) {
                    element["FARE"] = fare_data["PRICE"];
                });
                res.send(api_eta["data"]["data"]);
            }
        }
    } else if (route_data["COMPANY_CODE"] == "KMB") {
        const route_name = route_data["ROUTE_NAMEC"];

        const route = (await kmb.getRoutes(route_name)).find(route => route.bound === route_seq);
        if (route === undefined) {
            res.send("[]");
        } else {
            const variant = (await route.getVariants()).sort((a, b) => a.serviceType - b.serviceType)[0];

            if (variant === undefined) {
                res.send("[]");
            } else {
                const stoppings = await variant.getStoppings();
                const stopping = stoppings.find(stopping => stopping["sequence"] === stop_seq);
                
                if (stopping === undefined) {
                    res.send("[]");
                } else {
                    const etas = await stopping.getEtas();
                    res.send(etas);
                }
            }
        }
    } else {
        res.send("[]");
    }
});

function oppositeSEQ(seq) {
    return seq === 1 ? 2 : 1;
}

module.exports = router;
