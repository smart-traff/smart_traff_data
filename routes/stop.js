var express = require("express");
var router = express.Router();
const Client = require("../database/Client");
const Route_DB = require("../database/Route_DB");
const Rstop_DB = require("../database/Rstop_DB");
const Stop_DB = require("../database/Stop_DB");

const client = new Client();
const route_db = new Route_DB();
const rstop_db = new Rstop_DB();
const stop_db = new Stop_DB();

(async () => {
    await client.connect();    
    route_db.connect(client);
    rstop_db.connect(client);
    stop_db.connect(client);
})();

router.get('/', async (req, res) => {
    res.send(await stop_db.getAll());
});

router.get('/:STOP_ID', async (req,res) => {
    res.send(await stop_db.getByID(req.params.STOP_ID));
});

router.get("/:COMPANY_CODE/:ROUTE_NAME", async (req, res) => {
    const route = await route_db.getByCompanyCode_Name(req.params.COMPANY_CODE, req.params.ROUTE_NAME);

    if (route != null) {
        var response = {};
        response[route["COMPANY_CODE"]] = [];

        var rstop_seq_1 = await rstop_db.getByRouteID_RouteSeq(route["ROUTE_ID"], 1);
        var rstop_seq_2 = await rstop_db.getByRouteID_RouteSeq(route["ROUTE_ID"], 2);

        for (i = 0; i < rstop_seq_1.length; i += rstop_seq_1.length - 1) {
            const stop = await stop_db.getByID(rstop_seq_1[i].STOP_ID);

            var data = {
                Name: route.ROUTE_NAMEC,
                STOP_ID: rstop_seq_1[i].STOP_ID,
                ROUTE_SEQ: rstop_seq_1[i].ROUTE_SEQ,
                STOP_SEQ: rstop_seq_1[i].STOP_SEQ,
                STOP_NAMEC: rstop_seq_1[i].STOP_NAMEC,
                N: stop.N,
                E: stop.E,
            };
            response[route["COMPANY_CODE"]].push(data);
        }
        for (i = 0; i < rstop_seq_2.length; i += rstop_seq_2.length - 1) {
            const stop = await stop_db.getByID(rstop_seq_2[i].STOP_ID);

            var data = {
                Name: route.ROUTE_NAMEC,
                STOP_ID: rstop_seq_2[i].STOP_ID,
                ROUTE_SEQ: rstop_seq_2[i].ROUTE_SEQ,
                STOP_SEQ: rstop_seq_2[i].STOP_SEQ,
                STOP_NAMEC: rstop_seq_2[i].STOP_NAMEC,
                N: stop.N,
                E: stop.E,
            };
            response[route["COMPANY_CODE"]].push(data);
        }
        res.send(response);
    } else {
        res.send("[]");
    }
});

router.get("/:COMPANY_CODE/:ROUTE_NAME/:ROUTE_SEQ", async (req, res) => {
    const route = await route_db.getByCompanyCode_Name(req.params.COMPANY_CODE, req.params.ROUTE_NAME);

    if (route != null) {
        var response = {};
        response[route["COMPANY_CODE"]] = [];

        const rstops = await rstop_db.getByRouteID_RouteSeq(route["ROUTE_ID"], req.params.ROUTE_SEQ);

        for (i in rstops) {
            const busStoplocation = await stop_db.getByID(rstops[i].STOP_ID);

            var data = {
                Name: route.ROUTE_NAMEC,
                STOP_ID: rstops[i].STOP_ID,
                ROUTE_SEQ: rstops[i].ROUTE_SEQ,
                STOP_SEQ: rstops[i].STOP_SEQ,
                STOP_NAMEC: rstops[i].STOP_NAMEC,
                N: busStoplocation.N,
                E: busStoplocation.E,
            };
            response[route["COMPANY_CODE"]].push(data);
        }
        res.send(response);
    } else {
        res.send("[]");
    }
});

module.exports = router;
