var express = require("express");
var router = express.Router();
const SectionFare_DB = require("../database/SectionFare_DB");
const Client = require("../database/Client");

const client = new Client();
const sectionFare_db = new SectionFare_DB();

(async () => {
    await client.connect();
    sectionFare_db.connect(client);
})();

router.get("/:routeID/:routeSeq/:on/:off", async (req, res) => {
    res.send(await sectionFare_db.getByOnOffSeq(req.params.routeID, req.params.routeSeq, req.params.on, req.params.off));
});

module.exports = router;