var express = require("express");
var router = express.Router();
const axios = require("axios");
const parseString = require("xml2js").Parser({explicitArray : false}).parseString;
const Speedmap_Info_DB = require("../database/Speedmap_Info_DB");
const Client = require("../database/Client");

const client = new Client();
const speedmap_info_db = new Speedmap_Info_DB();

(async () => {
    await client.connect();
    speedmap_info_db.connect(client);
})();

const url = "https://resource.data.one.gov.hk/td/speedmap.xml";

router.get("/", async (req, res) => {
    const xml_data = await axios.get(url, {'Content-Type': 'text/xml'});

    parseString(xml_data["data"], function (err, result) {
        res.send(result["jtis_speedlist"]["jtis_speedmap"]);
    });
});

router.get("/:LINK_ID", async (req, res) => {
    res.send(await speedmap_info_db.getByLinkID(req.params.LINK_ID));
});

module.exports = router;
