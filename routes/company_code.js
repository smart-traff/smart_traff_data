var express = require("express");
var router = express.Router();
const Company_Code_DB = require("../database/Company_Code_DB");
const Client = require("../database/Client");

const client = new Client();
const company_code_db = new Company_Code_DB();

(async () => {
    await client.connect();
    company_code_db.connect(client);
})();

router.get("/", async (req, res) => {
    res.send(await company_code_db.getAll());
});

router.get("/:COMPANY_CODE", async (req, res) => {
    res.send(await company_code_db.getByCompanyCode(req.params.COMPANY_CODE));
});

module.exports = router;
