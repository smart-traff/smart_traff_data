var express = require("express");
var router = express.Router();
const Route_DB = require("../database/Route_DB");
const Client = require("../database/Client");

const client = new Client();
const route_db = new Route_DB();

(async () => {
    await client.connect();
    route_db.connect(client);
})();

router.get("/", async (req, res) => {
    res.send(await route_db.getAll());
});

router.get("/:ROUTE_ID", async (req, res) => {
    res.send(await route_db.getByID(req.params.ROUTE_ID));
});

router.get("/company_code/:COMPANY_CODE", async (req, res) => {
    res.send(await route_db.getByCompanyCode(req.params.COMPANY_CODE));
});

router.get("/name/:ROUTE_NAME", async (req, res) => {
    res.send(await route_db.getByName(req.params.ROUTE_NAME));
});

module.exports = router;
