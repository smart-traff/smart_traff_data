const {MongoClient} = require('mongodb');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
require('dotenv/config');
app.use(bodyParser.json());

const https = require('https');
var proj4 = require('proj4');
var result;
proj4.defs("EPSG:2326","+proj=tmerc +lat_0=22.31213333333334 +lon_0=114.1785555555556 +k=1 +x_0=836694.05 +y_0=819069.8 +ellps=intl +towgs84=-162.619,-276.959,-161.764,0.067753,-2.24365,-1.15883,-1.09425 +units=m +no_defs");
proj4.defs("EPSG:4326","+proj=longlat +datum=WGS84 +no_defs");
let url = "https://api.data.gov.hk/v2/filter?q=%7B%22resource%22%3A%22http%3A%2F%2Fstatic.data.gov.hk%2Ftd%2Froutes-and-fares%2FSTOP_BUS.mdb%22%2C%22section%22%3A1%2C%22format%22%3A%22json%22%7D";

https.get(url,(res)=>{
    let json_data = "";
    //get the data 
    res.on("data",(locationbus)=>{
        json_data +=locationbus;
    });
    //the data change the json format
    res.on("end",()=>{
        try{
            let json = JSON.parse(json_data);
            // call display method
            module.exports.display(json);
        }catch(error){
            console.error(error.message);
        }
    });


}).on("error",(error)=>{
    console.error(error.message);
   });


   module.exports = {
    display:function(json){
        var data = []

        for(i in json){  
           result = proj4('EPSG:2326', 'EPSG:4326', [json[i].X, json[i].Y]);

       
             var obj = {
                  STOP_ID:json[i].STOP_ID,
                  N: result[1],
                  E: result[0]
              }

               data.push(obj)

        }
        async function main() {
            var url = "mongodb+srv://ccfchan:0gkkvm123@cluster0.irxvt.mongodb.net/Test1?retryWrites=true&w=majority";
            var client = new MongoClient(url);
            try{
                await client.connect();
                await createMultipleListings(client,data);
            }catch(e){
                console.error(e);
            }
            finally{
                await client.close()
            }
        
        }
        main().catch(console.err);
        
        
        async function createMultipleListings(client, data){
            const result = await client.db("smart-traffic-testdb").collection("BusStoplocation").insertMany(data);
            console.log(`${data.i} new listing(s) created with the following id(s):`);
            console.log(data.i);
        }
     
    }
};
