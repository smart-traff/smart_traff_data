const Downloader = require("nodejs-file-downloader");
const ADODB = require("node-adodb");
const { MongoClient } = require("mongodb");
var xlsx = require("xlsx");

module.exports = {
    update_gov_data() {
        const company_code_mdb =
            "http://static.data.gov.hk/td/routes-and-fares/COMPANY_CODE.mdb";
        const route_bus_mdb =
            "http://static.data.gov.hk/td/routes-and-fares/ROUTE_BUS.mdb";
        const stop_bus_mdb =
            "http://static.data.gov.hk/td/routes-and-fares/STOP_BUS.mdb";
        const rstop_bus_mdb =
            "http://static.data.gov.hk/td/routes-and-fares/RSTOP_BUS.mdb";
        const fare_bus_mdb =
            "http://static.data.gov.hk/td/routes-and-fares/FARE_BUS.mdb";
        const speedmap_info =
            "http://static.data.gov.hk/td/traffic-speed-map/tc/tsm_link_and_node_info_v2.xls";

        new Downloader({
            url: company_code_mdb,
            directory: "./gov_data/",
            fileName: "company_code.mdb",
            cloneFiles: false,
        }).download();

        new Downloader({
            url: route_bus_mdb,
            directory: "./gov_data/",
            fileName: "route_bus.mdb",
            cloneFiles: false,
        }).download();

        new Downloader({
            url: stop_bus_mdb,
            directory: "./gov_data/",
            fileName: "stop_bus.mdb",
            cloneFiles: false,
        }).download();

        new Downloader({
            url: rstop_bus_mdb,
            directory: "./gov_data/",
            fileName: "rstop_bus.mdb",
            cloneFiles: false,
        }).download();

        new Downloader({
            url: fare_bus_mdb,
            directory: "./gov_data/",
            fileName: "fare_bus.mdb",
            cloneFiles: false,
        }).download();

        new Downloader({
            url: speedmap_info,
            directory: "./gov_data/",
            cloneFiles: false,
        }).download();
    },
    async insert_data() {
        const dbName = "smart-traffic-testdb";
        const url =
            "mongodb+srv://ccfchan:0gkkvm123@cluster0.irxvt.mongodb.net/Test1?retryWrites=true&w=majority";

        const client = new MongoClient(url);

        const company_code_connection = ADODB.open(
            "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=./gov_data/company_code.mdb;"
        );
        const route_bus_connection = ADODB.open(
            "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=./gov_data/route_bus.mdb;"
        );
        const rstop_bus_connection = ADODB.open(
            "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=./gov_data/rstop_bus.mdb;"
        );
        const stop_bus_connection = ADODB.open(
            "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=./gov_data/stop_bus.mdb;"
        );
        // const fare_bus_connection = ADODB.open(
        //     "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=./gov_data/fare_bus.mdb;"
        // );

        const company_code = await company_code_connection.query(
            "SELECT * FROM COMPANY_CODE"
        );
        const route = await route_bus_connection.query("SELECT * FROM ROUTE");
        const rstop = await rstop_bus_connection.query("SELECT * FROM RSTOP");
        const stop = await stop_bus_connection.query("SELECT * FROM STOP");
        // const fare = await fare_bus_connection.query("SELECT * FROM FARE");

        try {
            await client.connect();
            console.log("Connected correctly to server");
            const db = client.db(dbName);

            const company_code_col = db.collection("company_code");
            const route_col = db.collection("route");
            const rstop_col = db.collection("rstop");
            const stop_col = db.collection("stop");
            const fare_col = db.collection("fare");

            await company_code_col.drop();
            await company_code_col.insertMany(company_code);

            await route_col.drop();
            await route_col.insertMany(route);

            await rstop_col.drop();
            await rstop_col.insertMany(rstop);

            await stop_col.drop();
            await stop_col.insertMany(stop);

            // await fare_col.drop();
            // await fare_col.insertMany(fare);
        } catch (err) {
            console.log(err.stack);
        } finally {
            await client.close();
        }
    },
    async insertSpeedMap_Info() {
        try {
            const url =
                "mongodb+srv://ccfchan:0gkkvm123@cluster0.irxvt.mongodb.net/Test1?retryWrites=true&w=majority";

            const client = new MongoClient(url);
            await client.connect();
            var collection = client
                .db("smart-traffic-testdb")
                .collection("speedmap_info");

            const workbook = xlsx.readFile(
                "./gov_data/tsm_link_and_node_info_v2.xls"
            );
            const sheetNames = workbook.SheetNames;
            const worksheet = workbook.Sheets[sheetNames[0]];
            const headers = {};
            const data = [];
            const keys = Object.keys(worksheet);
            keys.forEach((k) => {
                let col = k.substring(0, 1);
                let row = parseInt(k.substring(1));
                let value = worksheet[k].v;

                if (row === 1) {
                    headers[col] = value.split("\n")[0];
                    return;
                }

                if (!data[row]) {
                    data[row] = {};
                }
                data[row][headers[col]] = value;
            });
            data.shift();
            data.shift();

            await collection.drop();
            await collection.insertMany(data);
        } catch (err) {
            console.log(err.stack);
        } finally {
            await client.close();
        }
    },
};
